from django.shortcuts import render

# Create your views here.
from django.views.generic import DetailView

from .models import BlogPost


class BlogPostDetail(DetailView):
    model = BlogPost
    template_name = 'Blog/detail.html'
