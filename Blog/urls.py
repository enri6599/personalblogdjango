from django.urls import include, path
from . import views, admin
from .views import BlogPostDetail

app_name = 'Blog'

urlpatterns = [
    path('blogpost/<int:pk>/detail', BlogPostDetail.as_view(), name='BlogPost-Detail')
]
