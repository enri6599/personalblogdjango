from django.db import models

# Create your models here.
from django.db import models


class BlogPost(models.Model):
    author = models.CharField(max_length=60)
    title = models.CharField(max_length=255)
    content = models.TextField()
    views = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.title} -id: {self.id}"
